;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
	mov &0x0216, r15
	mov #0x1234, r14
	mov #0x1000, r13
	cmp r14, r15
	jhs greater
	cmp r13, r15
	jlo less
	jmp great
greater
	mov #0x0014, r12
	mov #0x0014, r11
loop
	dec r11
	add r11, r12
	mov #0x0000, r10
	cmp r11, r10
	jn loop
	jmp forever
less
	mov r15, r8
	and #0x0001, r15
	jz even
	jnz odd
even
	rra r8
	jmp forever
odd
	rla r8
	jmp forever
great
	add #0xEEC0, r15
	mov #0x0000, &0x0202
	dadc &0x0202
	jmp forever
forever
	jmp forever
;---------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
